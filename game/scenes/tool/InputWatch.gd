extends Node

onready var _world = $"/root".get_child(0)

export var action = ""
export var timeout = 0.2
export var one_shot = false
export var ignores_pause = false

var has_fired = false

signal fire

func _ready():
	$Timer.wait_time = timeout

func _process(delta):
	if action != "" and Input.is_action_just_pressed(action) and $Timer.is_stopped() and (!one_shot or !has_fired) and (!_world.paused or ignores_pause):
		has_fired = true
		$Timer.start()
		emit_signal("fire")
		
func reset(act):
	if act: action = act
	has_fired = false
	
	