extends Node

export (String) var identifier = "Dialogue/"

var controller
var literal = false

signal done

# helper functions to use in implementations

func player(key):
	controller.show_player_dialogue((identifier if !literal else "") + key, literal)
	
func npc(key):
	controller.show_npc_dialogue((identifier if !literal else "") + key, literal)

func player_choice(choices):
	if literal: controller.give_player_choices(choices, true)
	else: 
		var new_choices = []
		for ch in choices:
			new_choices.push_back(identifier + ch)
		controller.give_player_choices(new_choices, false)
	
func done():
	emit_signal("done")

# to be overridden by implementations

func begin():
	pass

func finish(world):
	pass
	
func postdialogue(player):
	return false