extends Node2D

signal begin
signal finished
signal next
signal transition_ok
signal key

const TEXT_FADE_SPEED = 0.3

export (NodePath) var __activation_zone # Area2D
export (NodePath) var __chat_icon_position # Position2D
export (NodePath) var __player_text_container # Position2D
export (NodePath) var __npc_text_container # Position2D
export (NodePath) var __camera # Camera2D
export (NodePath) var __player # Player
export (NodePath) var __transition_scrim # TransitionScrim
export (int) var player_position
export (NodePath) var __dialogue_provider # (DialogueProvider)

export (String, "None", "C", "H", "N", "O") var npc_text_color
export (String, "left", "right") var player_direction

export (bool) var listening = true setget _set_listening, _get_listening
func _set_listening(l):
	listening = l
	if (!l):
		_active = false
		$ChatIconSprite.visible = false
func _get_listening():
	return listening

var _active = false
var _in_progress = false

var _last_player_choice = -1
func get_last_choice():
	return _last_player_choice

# resolve all nodepaths
onready var activation_zone = get_node(__activation_zone)
onready var chat_icon_position = get_node(__chat_icon_position)
onready var player_text_container = get_node(__player_text_container)
onready var npc_text_container = get_node(__npc_text_container)
onready var camera = get_node(__camera)
onready var player = get_node(__player)
onready var transition_scrim = get_node(__transition_scrim)
onready var dialogue_provider = get_node(__dialogue_provider)

onready var int_npc_label = $InternalNPCText
onready var int_player_container = $InternalPlayerContainer
onready var int_player_label = $InternalPlayerContainer/InternalPlayerText

onready var player_arr_left = $InternalPlayerContainer/ArrowLeft
onready var player_arr_right = $InternalPlayerContainer/ArrowRight

onready var npc_animator = $InternalNPCText/AnimationPlayer
onready var player_animator = $InternalPlayerContainer/AnimationPlayer

onready var _world = $"/root".get_child(0)
onready var COLOR = _world.get_node("ColorDict")

func _ready():
	activation_zone.connect("body_entered", self, "_player_here")
	activation_zone.connect("body_exited", self, "_player_gone")
	$ChatIconSprite.position = chat_icon_position.position
	
	# set animation speed
	player_animator.playback_speed = 1 / TEXT_FADE_SPEED
	npc_animator.playback_speed = 1 / TEXT_FADE_SPEED
	
	$KeyRight.timeout = TEXT_FADE_SPEED
	$KeyLeft.timeout = TEXT_FADE_SPEED
	
	# move containers into placeholders
	remove_child(int_npc_label)
	npc_text_container.add_child(int_npc_label)
	remove_child(int_player_container)
	player_text_container.add_child(int_player_container)
	
	int_npc_label.rect_size = npc_text_container.rect_size * 2
	int_player_container.rect_size = npc_text_container.rect_size * 2
	
	int_player_label.add_color_override("font_color", COLOR.O_LIGHT)
#	player_arr_left.modulate = COLOR.O_LIGHT
#	player_arr_right.modulate = COLOR.O_LIGHT
	if npc_text_color != "None":
		int_npc_label.add_color_override("font_color", COLOR[npc_text_color + "_LIGHT"])
	
	npc_text_container.visible = false
	player_text_container.visible = false
	
	dialogue_provider.controller = self

func _process(delta):
	if (_active and !_in_progress and Input.is_action_pressed("game_action")):
		# here we go! it's dialogue time!
		_activate_dialogue()

func _player_here(body):
	if (body != player): return
	if (!listening or _in_progress): return
	_active = true
	_world.dialogue_zone_enter()
	$ChatIconSprite.visible = true
	$ChatIconSprite.play("appear")
	yield($ChatIconSprite, "animation_finished")
	if (_active): # make sure the player hasn't already left
		$ChatIconSprite.play("idle")
	
func _player_gone(body):
	if (body != player): return
	_active = false
	if (!listening or _in_progress): return
	_world.dialogue_zone_exit()
	$ChatIconSprite.play("disappear")
	yield($ChatIconSprite, "animation_finished")
	if (!_active): # make sure the player is still gone
		$ChatIconSprite.visible = false
		
func _activate_dialogue():
	_in_progress = true
	
	# immediately stop user movement
	player.ignore_input = true
	
	# prime animations - this seems to help them not ficker the first time
	# also will ensure they end up invisible
	int_npc_label.text = ""
	int_player_label.text = ""
	npc_animator.play_backwards("fade_in")
	player_animator.play_backwards("fade_in")
	npc_text_container.visible = true
	player_text_container.visible = true
	
	# bring up the scrim
	transition_scrim.obscure()
	yield(transition_scrim, "done")
	
	# alert the main scene
	_world.dialogue_scene_begin()
	emit_signal("begin")
	
	# switch to our camera
	camera.make_current()
	
	# face the player
	if player_direction == "left": player.face_left()
	else: player.face_right()
	
	# clean up the scene
	$ChatIconSprite.visible = false
	player.force_jump_to(player_position, true)
	
	# back to the world...
	transition_scrim.reveal()
	yield(transition_scrim, "done")
	
	# small delay...
	$DelayTimer.start()
	yield($DelayTimer, "timeout")
	
	# run the dialogue sequence
	dialogue_provider.begin()
	yield(dialogue_provider, "done")
	
	# delay again
	$DelayTimer.start()
	yield($DelayTimer, "timeout")
	
	# scrim up
	transition_scrim.obscure()
	yield(transition_scrim, "done")
	
	# alert the main scene
	dialogue_provider.finish()
	_world.dialogue_scene_end()
	emit_signal("finished")
	player.use_camera()
	
	# scrim reveal
	transition_scrim.reveal()
	yield(transition_scrim, "done")
	
	# turn this scene off
	listening = false
	_in_progress = false
	_active = false
	
	# let dialogue provider take postdialogue actions on the player
	var withhold_control = dialogue_provider.postdialogue()
	
	# give the player control, unless the dialogue provider
	# asked to hold on to it
	if !withhold_control: player.ignore_input = false


func prepare_key():
	$KeyAction.reset("game_action")
	
func delay_prepare_key():
	$KeyTimeout.start()

func _on_KeyTimeout_timeout():
	$KeyAction.reset("game_action")
	
func get_world():
	return _world
func get_player():
	return player

############ DIALOGUE CONTROL

func show_player_dialogue(key, is_literal):
	if (is_literal): int_player_label.text = key
	else: int_player_label.use(key)
	player_animator.play("fade_in")
	yield(player_animator, "animation_finished")
	prepare_key()
	yield($KeyAction, "fire")
	player_animator.play_backwards("fade_in")
	yield(player_animator, "animation_finished")
	emit_signal("next")
	pass

func show_npc_dialogue(key, is_literal):
	if (is_literal): int_npc_label.text = key
	else: int_npc_label.use(key)
	npc_animator.play("fade_in")
	yield(npc_animator, "animation_finished")
	prepare_key()
	yield($KeyAction, "fire")
	npc_animator.play_backwards("fade_in")
	yield(npc_animator, "animation_finished")
	emit_signal("next")
	pass

var choice_active = false
var player_choices
var current_choice = -1
var accepting_action = false
func give_player_choices(choices, is_literal):
	choice_active = true
	player_choices = choices
	current_choice = 0
	int_player_label.modulate = Color(1, 1, 1, 1)
	if (is_literal): int_player_label.text = choices[0]
	else: int_player_label.use(choices[0])
	player_arr_right.visible = true
	player_animator.play("fade_in")
	yield(player_animator, "animation_finished")
	
	# action key listener is delayed, so a player rapidly clicking
	# through dialogue is less likely to miss choices
	delay_prepare_key()
	
	var key = -1
	while key != 0:
		accepting_action = true
		key = yield(self, "key")
		accepting_action = false
		if key != 0:
			current_choice += key
			if current_choice == -1: current_choice = player_choices.size() - 1
			if current_choice == player_choices.size(): current_choice = 0
			player_animator.play("fade_out_text")
			yield(player_animator, "animation_finished")
			if (is_literal): int_player_label.text = choices[current_choice]
			else: int_player_label.use(choices[current_choice])
			player_arr_left.visible = current_choice != 0
			player_arr_right.visible = current_choice != player_choices.size() - 1
			player_animator.play_backwards("fade_out_text")
	
	# we've made a choice!
	player_animator.play_backwards("fade_in")
	yield(player_animator, "animation_finished")
	
	player_arr_left.visible = false
	player_arr_right.visible = false
	
	_last_player_choice = current_choice
	emit_signal("next", current_choice)

func _on_KeyLeft_fire():
	emit_signal("key", -1)
func _on_KeyRight_fire():
	emit_signal("key", 1)
func _on_KeyAction_fire():
	if !accepting_action: prepare_key()
	else: emit_signal("key", 0)