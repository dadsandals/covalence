extends Area2D

signal body_entered_once(body)

var has_fired = false

func reset():
	has_fired = false

func _on_Area2D_body_entered(body):
	if !has_fired:
		emit_signal("body_entered_once", body)
		has_fired = true