extends RichTextLabel

export var key = "" setget _set_key, _get_key
export (bool) var force_centering = false

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

onready var dict = $"/root".get_child(0).get_node("Dictionary")

func _ready():
	update()
	
func update():
	if !dict or !key: return
	if bbcode_enabled: 
		var txt = dict.resolve(key)
		if force_centering:
			txt = "[center]" + txt + "[/center]"
		bbcode_text = txt
	else: text = dict.resolve(key)

func use(k):
	self.key = k

func _set_key(k):
	key = k
	update()

func _get_key():
	return key
