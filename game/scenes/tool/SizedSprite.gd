extends Sprite

export (float) var radius = 8 setget set_radius, get_radius
export (Resource) var texture_left
export (Resource) var texture_right


func set_radius(r):
	radius = r
	update_display()
func get_radius():
	return radius
	
var ready = false
func _ready():
	ready = true

func update_display():
	if not ready: return
	var new_scale = 2.0 * radius / texture.get_width()
	scale = Vector2(new_scale, new_scale)
	
func face_left():
	texture = texture_left
	
func face_right():
	texture = texture_right