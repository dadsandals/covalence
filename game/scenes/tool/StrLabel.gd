extends Label

export var key = "" setget _set_key, _get_key

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

onready var dict = $"/root".get_child(0).get_node("Dictionary")

func _ready():
	update()
	
func update():
	if !dict or !key: return
	text = dict.resolve(key)

func use(k):
	self.key = k

func _set_key(k):
	key = k
	update()

func _get_key():
	return key
