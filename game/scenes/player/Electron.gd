extends KinematicBody2D

export (float) var radius = 8 setget set_radius, get_radius
func set_radius(r):
	radius = r
	update_display()
func get_radius():
	return radius
	
export (float) var orbital_radius = 72
export (float) var orbital_speed = 0.35
export (float) var orbital_angle = 0

var ready = false

onready var sprite = $Sprite
onready var colshape = $CollisionShape2D.shape

func _ready():
	ready = true
	update_display()
	
func update_display():
	if not ready: return
	sprite.radius = radius
	colshape.radius = radius
	
func _process(delta):
  orbital_angle += delta * orbital_speed
  var pos = Vector2()
  pos.x = orbital_radius * cos(orbital_angle)
  pos.y = orbital_radius * sin(orbital_angle)
  position = pos
	