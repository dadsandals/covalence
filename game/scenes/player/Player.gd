extends KinematicBody2D

export (float) var radius = 36 setget set_radius, get_radius
func set_radius(r):
	radius = r
	update_display()
func get_radius():
	return radius

export (float) var electron_radius = 8 setget set_erad, get_erad
func set_erad(r):
	electron_radius = r
	update_display()
func get_erad():
	return electron_radius
export var electron_orbital_radius = [72, 78]
export (float) var electron_orbital_speed = 0.35

export (bool) var bob = true
export (float) var bob_amount = 6
export (float) var bob_speed = 0.3
var bob_theta = 0.0

var ready = false
var electrons_placed = false

var follow_path = false
var path_follower
var vel = 0.0
var pos = 0.0

export (float) var speed = 1.0
export (float) var damp = 0.5

export (float) var bounce = 0.75

export (bool) var moves_parent = true
export (bool) var ignore_input = false
var sync_motion = false

export (float) var world_speed = 1.0 setget set_ws, get_ws
func set_ws(s):
	world_speed = s
	if not ready: return
	electrons[0].orbital_speed = world_speed * electron_orbital_speed
	electrons[1].orbital_speed = world_speed * electron_orbital_speed
	$Camera2D.smoothing_speed = 0.8 * world_speed
func get_ws():
	return world_speed

# path bounds
export var position_min = -1
export var position_max = -1
signal hit_bound
signal hit_min
signal hit_max

onready var col = $CollisionShape2D
onready var colshape = col.shape
onready var transformer = $Transform
onready var sprite = $Transform/Sprite
onready var electrons = [$Transform/Electron1, $Transform/Electron2]

onready var _world = $"/root".get_child(0)
onready var COLOR = _world.get_node("ColorDict")

func _ready():
	ready = true
	if moves_parent:
		move_parent()
	update_display()
	electrons[0].modulate = COLOR.O_DARK
	electrons[1].modulate = COLOR.O_DARK
	
func update_display():
	if not ready: return
	sprite.radius = radius
	colshape.radius = radius
	$Camera2D.position = Vector2(0, 0)
	for i in range(2):
		var electron = electrons[i]
		electron.orbital_radius = electron_orbital_radius[i]
		electron.orbital_speed = world_speed * electron_orbital_speed
		if not electrons_placed:
			electron.orbital_angle = PI * i

func _process(delta):
	var bob_disp = Vector2()
	if (bob):
		bob_theta += delta * world_speed * bob_speed
		bob_disp.y = sin(bob_theta) * bob_amount
	col.position = bob_disp
	transformer.position = bob_disp
	
	if follow_path and !ignore_input:
		var dir = 0
		if Input.is_action_pressed("move_forward"):
			dir += 1
		if Input.is_action_pressed("move_backward"):
			dir -= 1
		vel += dir * delta * speed * world_speed
		
		var last_x = path_follower.position.x
		
		path_follower.offset = pos
		if sync_motion: position = path_follower.position
		
		if (path_follower.position.x < last_x):
			sprite.face_left()
		if (path_follower.position.x > last_x):
			sprite.face_right()
		if path_follower.unit_offset > 1.0 or (position_max != -1 and pos < position_max):
			vel = -bounce * abs(vel)
			emit_signal("hit_max")
			emit_signal("hit_bound")
		if path_follower.unit_offset < 0 or (position_min != -1 and pos < position_min):
			vel = bounce * abs(vel)
			emit_signal("hit_min")
			emit_signal("hit_bound")
			
func force_jump_to(new_pos, clear_vel):
	pos = new_pos
	if clear_vel: vel = 0
	path_follower.offset = pos
	if sync_motion: position = path_follower.position
		
func _physics_process(delta):
	if world_speed > 0:
		vel *= pow(damp, delta * world_speed)
		pos += vel * world_speed

func follow(pathfollow):
	if pathfollow is PathFollow2D:
		follow_path = true
		path_follower = pathfollow
		sync_motion = true
	
func move_parent():
	if get_parent() is PathFollow2D:
		follow_path = true
		path_follower = get_parent()
		sync_motion = false
		position = Vector2(0, 0)
		
func use_camera():
	$Camera2D.make_current()
	
func face_left():
	sprite.face_left()

func face_right():
	sprite.face_right()
	
func bond():
	$Transform/Electron1.visible = false
	$Transform/Electron2.visible = false
	$Transform/Bond.visible = true