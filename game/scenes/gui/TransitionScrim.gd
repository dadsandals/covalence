extends CanvasLayer

export var duration = 0.25

signal done
signal done_obscuring
signal done_revealing

var is_transitioning = false
var is_obscured = false

func _ready():
	$AnimationPlayer.playback_speed = 0.5 / duration
	
func obscure_color(clr):
	$AnimationPlayer/ColorRect.color = clr
	obscure()
	
func obscure():
	$AnimationPlayer/ColorRect.visible = true
	$AnimationPlayer.play("obscure")
	yield($AnimationPlayer, "animation_finished")
	emit_signal("done")
	emit_signal("done_obscuring")

func reveal():
	$AnimationPlayer/ColorRect.visible = true
	$AnimationPlayer.play_backwards("obscure")
	yield($AnimationPlayer, "animation_finished")
	$AnimationPlayer/ColorRect.visible = false
	emit_signal("done")
	emit_signal("done_revealing")