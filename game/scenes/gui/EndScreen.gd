extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func show(key):
	$Mod/StrLabel.use("Endings/" + key)
	$Mod/AnimationPlayer.play("show")

func _on_AgainButton_pressed():
	get_tree().change_scene("res://scenes/world/World.tscn")
	pass # replace with function body

func _on_CreditsButton_pressed():
	$Mod/StrLabel.use("Endings/CREDITS")
	$Mod/HBoxContainer/CreditsButton.visible = false