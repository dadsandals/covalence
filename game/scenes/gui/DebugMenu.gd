extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"


func _ready():
	$Mod.visible = false

func _on_MenuOpen_fire():
	$Mod.visible = !$Mod.visible

func _on_WSpeedSlide_value_changed(value):
	$Mod/MarginContainer/MarginContainer/GridContainer/WSpeedValue.text = str(value)
	$"/root".get_node("World").world_energy = value

func _on_CameraX_value_changed(value):
	$"/root".get_child(0).get_node("PlayerPath/PathFollow2D/Player/Camera2D").offset.x = value

func _on_CameraY_value_changed(value):
	$"/root".get_child(0).get_node("PlayerPath/PathFollow2D/Player/Camera2D").offset.y = value

func _on_CameraZoom_value_changed(value):
	$"/root".get_child(0).get_node("PlayerPath/PathFollow2D/Player/Camera2D").zoom = Vector2(value, value)
