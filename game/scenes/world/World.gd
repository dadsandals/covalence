extends Node

signal world_speed_change

# world state
var world_energy = 1.0 setget set_world_energy, get_world_energy
var saved_world_energy

var paused = false

var quick_end_ending = "NOINT"

const ANIM_SPEED = 0.3

func set_world_energy(energy):
	world_energy = energy 
	set_world_speed(energy)
	particles.get_node("ParticlesAbove")
	for part in moving_particles:
		part.speed_scale = pow(energy, 3)
	still_particles.speed_scale = energy
	emit_signal("world_speed_change", energy)
	
func get_world_energy():
	return world_energy

var should_hint_for_dialogue = false
var hinting_for_dialogue = false

onready var particles = $PlayerPath/PathFollow2D/Particles
onready var still_particles = $PlayerPath/PathFollow2D/Particles/ParticlesStill
onready var moving_particles = [
	$PlayerPath/PathFollow2D/Particles/ParticlesAbove,
	$PlayerPath/PathFollow2D/Particles/ParticlesRight,
	$PlayerPath/PathFollow2D/Particles/ParticlesBelow,
	$PlayerPath/PathFollow2D/Particles/ParticlesLeft
]

onready var pause_gui = $PauseMenu/Mod
onready var tween = $Tween

var offset = 0
func add_speed_offset(d):
	offset += d
	
func _process(delta):
	if offset != 0:
		self.world_energy = 1 + offset
		offset = 0

func _ready():
	
	particles.visible = true
	
	$PlayerPath/PathFollow2D/PlayerHint.use("Hints/KEY_ONWARD")
	$InputWatch.action = "move_forward"
	yield($InputWatch, "fire")
	hide_player_hint(false)
	yield($BackwardTrigger, "body_entered_once")
	$PlayerPath/PathFollow2D/PlayerHint.use("Hints/KEY_BACKWARD")
	show_player_hint()
	start_await_home()
	$InputWatch.reset("move_backward")
	yield($InputWatch, "fire")
	hide_player_hint(false)
	should_hint_for_dialogue = true

func start_await_home():
	yield($PlayerPath/PathFollow2D/Player, "hit_min")
	$Labels/LabelReturn.visible = true

func hide_player_hint(immediately):
	tween.interpolate_property($PlayerPath/PathFollow2D/PlayerHint, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.5, Tween.TRANS_SINE, Tween.EASE_OUT, 0 if immediately else 2)
	tween.start()
	
func show_player_hint():
	tween.interpolate_property($PlayerPath/PathFollow2D/PlayerHint, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 0.5, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.start()
	
func dialogue_zone_enter():
	if (should_hint_for_dialogue):
		$PlayerPath/PathFollow2D/PlayerHint.use("Hints/KEY_INTERACT")
		hinting_for_dialogue = true
		show_player_hint()
	
func dialogue_zone_exit():
	if (hinting_for_dialogue):
		hinting_for_dialogue = false
		hide_player_hint(true)
	
func dialogue_scene_begin():
	$PlayerPath/PathFollow2D/PlayerHint.visible = false
	should_hint_for_dialogue = false
	
func dialogue_scene_end():
	pass
	
	
func do_ending(ending_key):
	$PlayerPath/PathFollow2D/Player.ignore_input = true
	$EndScreen.show(ending_key)
	
func set_world_speed(speed):
	$PlayerPath/PathFollow2D/Player.world_speed = speed

func _on_Button_pressed():
	$PlayerPath/PathFollow2D/Player.force_jump_to(3000, false)
	$Button.release_focus() # so space in dialogue won't jump the player away

var unpausing = false
func _on_PauseWatch_fire():
	if !paused:
		pause_gui.visible = true
		saved_world_energy = world_energy
		tween.interpolate_property(self, "world_energy", saved_world_energy, 0.0, ANIM_SPEED * 1.5, Tween.TRANS_SINE, Tween.EASE_OUT)
		tween.interpolate_property(pause_gui, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), ANIM_SPEED, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	else:
		unpausing = true
#		pause_gui.mouse_filter = pause_gui.MOUSE_FILTER_PASS
		tween.interpolate_property(self, "world_energy", 0.0, saved_world_energy, ANIM_SPEED * 1.25, Tween.TRANS_SINE, Tween.EASE_IN)
		tween.interpolate_property(pause_gui, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), ANIM_SPEED, Tween.TRANS_LINEAR, Tween.EASE_IN, ANIM_SPEED * .25)
	tween.start()
	paused = !paused

func _on_Tween_tween_completed(object, key):
	if object == pause_gui and unpausing:
		paused = false
		unpausing = false
		pause_gui.visible = false

func _on_QuickEndTrigger_body_entered_once(body):
	do_ending(quick_end_ending)
