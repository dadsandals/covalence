extends Node2D

export (String, "left", "right") var facing = "right" setget _set_facing, _get_facing

func _set_facing(f):
	facing = f
	if facing == "left": face_left()
	else: face_right()
func _get_facing():
	return facing

func _ready():
	if facing == "left": face_left()

func face_left():
	for i in get_children():
		if i.has_method("face_left"): i.face_left()
	
func face_right():
	for i in get_children():
		if i.has_method("face_left"): i.face_right()