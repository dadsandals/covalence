extends "./facer.gd"

export var gawker = false

func _ready():
	$Nitrogen1.rotation = -rotation
	$Nitrogen2.rotation = -rotation
	
	if gawker:
		$Area2D.connect("body_entered", self, "_gawk")
		$Area2D.connect("body_exited", self, "_ungawk")

func _gawk(body):
	if facing == "right": face_left()
	else: face_right()
	
func _ungawk(body):
	if facing == "left": face_left()
	else: face_right()