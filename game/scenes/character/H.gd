extends "./facer.gd"

onready var _world = $"/root".get_child(0)
onready var COLOR = _world.get_node("ColorDict")

func _ready():
	$Electron.modulate = COLOR.H_DARK
	$Electron.orbital_angle = randf() * 2 * PI
	_world.connect("world_speed_change", self, "update_speed")
	
func update_speed(spd):
	$Electron.orbital_speed = 0.4 * spd