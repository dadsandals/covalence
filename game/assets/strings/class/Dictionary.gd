extends Node

var dict = {}

var _sub = false
var _sup

func dict_ident():
	pass

func _ready():
	if (get_parent().has_method("dict_ident")):
		_sub = true
		_sup = get_parent()
		for key in dict.keys():
			_sup.add(name + "/" + key, dict[key])

func resolve(key):
	return dict[key]

func add(key, val):
	if _sub: _sup.add(key, val)
	else: dict[key] = val