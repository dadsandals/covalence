extends Node

export var C_DARK = Color("b3c153")
export var C_LIGHT = Color("f0ff8c")
export var H_DARK = Color("504dbf")
export var H_LIGHT = Color("9997df")
export var N_DARK = Color("1eab74")
export var N_LIGHT = Color("8aeec7")
export var O_DARK = Color("00a4bc")
export var O_LIGHT = Color("7accc8")