extends "res://scenes/tool/dialogue/DialogueProvider.gd"

var choice

export (NodePath) var __main_hydrogen
onready var main_hydrogen = get_node(__main_hydrogen)

export (NodePath) var __friend_hydrogen
onready var friend_hydrogen = get_node(__friend_hydrogen)

func _ready():
	literal = true

func begin():
	player("hello?")
	yield(controller, "next")
	main_hydrogen.facing = "right"
	npc("oh!")
	yield(controller, "next")
	npc("wow, hello!")
	yield(controller, "next")
	npc("…")
	yield(controller, "next")
	npc("i’m sorry, this is a little forward…")
	yield(controller, "next")
	npc("but i really like your electrons.")
	yield(controller, "next")
	npc("can i share one?")
	yield(controller, "next")
	
	player_choice([
		"of course!",
		"no, what? that’s weird."
	])
	choice = yield(controller, "next")
	
	if (choice == 1):
		npc("oh… are you sure?")
		yield(controller, "next")
		player_choice([
			"…fine, whatever, we can share.",
			"i am absolutely positive."
		])
		choice = yield(controller, "next")
		
	if (choice == 1):
		player("go away.")
		yield(controller, "next")
		
		var anim = main_hydrogen.get_node("AnimationPlayer")
		anim.play("leave")
		yield(anim, "animation_finished")
		
		controller.get_world().do_ending("DONT_BOND")
		
		return
	
	npc("yay! i’m excited!")
	yield(controller, "next")
	npc("oh, one sec, i think my friend is interested in sharing your other electron…")
	yield(controller, "next")
	
	controller.get_player().face_right()
	
	var anim = friend_hydrogen.get_node("AnimationPlayer")
	anim.play("move")
	yield(anim, "animation_finished")
	
	$Timer.wait_time = 1
	$Timer.start()
	yield($Timer, "timeout")
	
	var scrim = controller.get_world().get_node("TransitionScrim")
	
	scrim.obscure_color(Color(1, 1, 1, 1))
	yield(scrim, "done")
	
	friend_hydrogen.visible = false
	main_hydrogen.visible = false
	
	$Timer.wait_time = 0.5
	$Timer.start()
	yield($Timer, "timeout")
	
	controller.get_player().bond()
	controller.get_player().face_left()
	
	controller.get_node("Camera2D").global_position = controller.get_player().global_position
	
	
	scrim.reveal()
	yield(scrim, "done")
	
	$Timer.wait_time = 2
	$Timer.start()
	yield($Timer, "timeout")
	controller.get_world().do_ending("SUCCESS")