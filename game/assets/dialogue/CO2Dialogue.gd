extends "res://scenes/tool/dialogue/DialogueProvider.gd"

var choice

var is_primed = false
var aboutbond = ""

enum endings {DRIFT, IGNORE, GETHELP}
var ending

export (NodePath) var __co2
onready var co2 = get_node(__co2)

func _ready():
	literal = true

func begin():
	ending = endings.IGNORE
	
	var dds = $"../../DiatomicDialogueScene"
	dds.listening = false
	dds.get_node("O2/AnimationPlayer").play("leave")
	
	
	if is_primed:
		aboutbond = "O₂ said you could tell me about bonding."
		
		player_choice([
			"hi",
			"are you O₂’s friend?",
			"…"
		])
		choice = yield(controller, "next")
		
		match choice:
			0: return _hi()
			1: return _friend()
			2: return _ignore()
		
	else:
		aboutbond = "could you tell me about bonding?"
		player_choice([
			"hi",
			"who are you?",
			"…"
		])
		choice = yield(controller, "next")
		
		match choice:
			0: return _hi()
			1: return _who()
			2: return _ignore()

func _hi():
	npc("…")
	yield(controller, "next")
	npc("hi.")
	yield(controller, "next")
	
	if is_primed:
		player_choice([
			"are you O₂’s friend?",
			"what, that’s it?",
			"…"
		])
		choice = yield(controller, "next")
		
		match choice:
			0: return _friend()
			1: return _hiend()
			2: return _ignore()
	else:
		player("what, that’s it?")
		yield(controller, "next")
		
		return _hiend()
	
func _hiend():
	npc("whaddaya want from me?")
	yield(controller, "next")
	npc("i don’t even know you, weirdo.")
	yield(controller, "next")
	
	ending = endings.IGNORE
	done()

func _friend():
	npc("sure, i know O₂. who’s askin’?")
	yield(controller, "next")
	return _ident()
	
func _who():
	npc("depends. who’s askin’?")
	yield(controller, "next")
	return _ident()
	
func _ident():
	player_choice([
		"i am oxygen.",
		"i dunno, who’s askin’?",
		"…"
	])
	choice = yield(controller, "next")
	match choice:
		0: return _oxygen()
		1: return _ident1()
		2: return _ignore()
	
	
func _oxygen():
	npc("oxygen. interestin’")
	yield(controller, "next")
	npc("i’m carbon dioxide. CO₂.")
	yield(controller, "next")
	npc("what was it that you wanted, again?")
	yield(controller, "next")
	
	player(aboutbond)
	yield(controller, "next")
	
	return _aboutbond()
	
func _aboutbond():
	npc("bonding? why do you wanna know about—")
	yield(controller, "next")
	npc("…ohhhh. weird. you’re by yourself, huh?")
	yield(controller, "next")
	npc("bummer.")
	yield(controller, "next")
	
	player_choice([
		"yeah, i guess…",
		"so i’ve been told.",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		0: return _trybond()
		1: return _skepbond()
		2: return _ignore()

func _ident1():
	npc("whaddaya mean, who’s askin’? i’m askin’ you who’s askin’.")
	yield(controller, "next")
	player_choice([
		"i’m askin’ you who’s askin’ me.",
		"aaaaaaah!",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		0: return _ident2()
		1: return _yell1()
		2: return _ignore()

func _ident2():
	npc("alright, this is getting confusing.")
	yield(controller, "next")
	npc("i’m carbon dioxide, but feel free to call me CO₂")
	yield(controller, "next")
	
	player_choice([
		"and i’m oxygen.",
		aboutbond,
		"…"
	])
	choice = yield(controller, "next")
	match choice:
		1: return _aboutbond()
		2: return _ignore()
		
	npc("alright, glad we got that out of the way.")
	yield(controller, "next")
	
	player(aboutbond)
	yield(controller, "next")
	
	return _aboutbond()

func _yell1():
	npc("aaaaaaaaaaaaah!")
	yield(controller, "next")
	
	player_choice([
		"aaaaaaaaaaaaaa aaaaaaaaah!",
		"calm down!",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		0: return _yell2()
		1: return _calm()
		2: return _ignore()
		
func _yell2():
	npc("oh, for the love of— who the heck are you?")
	yield(controller, "next")
	
	player_choice([
		"i am oxygen.",
		"your worst nightmare.",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		0: return _oxygen()
		2: return _ignore()
		
	npc("my wh— haha!")
	yield(controller, "next")
	npc("are you the heat death of the universe?")
	yield(controller, "next")
	
	player_choice([
		"no?",
		"hey, same worst nightmare!",
		"…"
	])
	choice = yield(controller, "next")
	
	if (choice == 2): return _ignore()
	
	if (choice == 1):
		npc("haha, yeah, the heat death of the universe is pretty freakin’ terrifying.")
		yield(controller, "next")
		npc("anyway, who are you?")
		yield(controller, "next")
		
		player("i am oxygen.")
		yield(controller, "next")
		
		return _oxygen()
		
	npc("are you asking me?")
	yield(controller, "next")
	npc("do you not know whether or not you’re the harbinger of the apocalypse?")
	yield(controller, "next")
	
	player_choice([
		"i’m confused.",
		"i am oxygen.",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		1: return _oxygen()
		2: return _ignore()
	
	npc("yeah, me too.")
	yield(controller, "next")
	npc("let’s start over.")
	yield(controller, "next")
	npc("who the heck are you?")
	yield(controller, "next")
	
	player("i am oxygen.")
	yield(controller, "next")
	
	return _oxygen()
	
func _calm():
	npc("calm down? you’re the one who was screaming!")
	yield(controller, "next")
	npc("okay, okay. who the heck are you, what the heck can i do for you?")
	yield(controller, "next")
	
	player("i am oxygen.")
	yield(controller, "next")
	
	return _oxygen()

func _trybond():
	npc("you’re tryin’ to figure out how to bond, the first step is findin’ other lonely molecules.")
	yield(controller, "next")
	npc("you aren’t gonna be able to bond with me, or O₂.")
	yield(controller, "next")
	npc("cause we don’t have our electrons just… hangin’ out there, you know?")
	yield(controller, "next")
	
	player_choice([
		"where am i supposed to find other lonely molecules?",
		"i’m really not sure…",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		0: return _find()
		1: return _skepbond()
		2: return _ignore()
	
func _skepbond():
	npc("…are you sure you wanna be bonded?")
	yield(controller, "next")
	npc("cause i mean, you don’t have to.")
	yield(controller, "next")
	npc("it’s not a very stable existence, especially you being oxygen and all.")
	yield(controller, "next")
	npc("but, you don’t have to.")
	yield(controller, "next")
	
	player_choice([
		"no, i wanna.",
		"maybe i don’t.",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		0: return _wantbond()
		1: return _dontbond()
		2: return _ignore()
	
func _wantbond():
	npc("okay.")
	yield(controller, "next")
	player("where am i supposed to find other lonely molecules?")
	yield(controller, "next")
	
	return _find()
	
func _dontbond():
	player("it’s a lot of pressure")
	yield(controller, "next")
	npc("then don’t.")
	yield(controller, "next")
	npc("drift on.")
	yield(controller, "next")
	npc("be unbonded.")
	yield(controller, "next")
	npc("maybe i’ll see you around sometime.")
	yield(controller, "next")
	
	ending = endings.DRIFT
	done()


func _find():
	npc("i can’t say for certain, but you gotta go where feels right.")
	yield(controller, "next")
	npc("where it feels a little more…")
	yield(controller, "next")
	npc("exciting.")
	yield(controller, "next")
	
	player_choice([
		"…okay.",
		"where things feel more ‘exciting’?",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		0: return _findokay()
		1: return _unhelp()
		2: return _ignore()
		
func _findokay():
	npc("that’s really all i can tell you.")
	yield(controller, "next")
	npc("the thing about bonding is you never really know when, or if, it’ll happen.")
	yield(controller, "next")
	npc("cause we’re all just playthings of fate, right?")
	yield(controller, "next")
	
	player("i guess?")
	yield(controller, "next")
	
	npc("alright. see ya, lil dude.")
	yield(controller, "next")
	npc("good luck.")
	yield(controller, "next")
	
	player("see you.")
	yield(controller, "next")
	
	ending = endings.GETHELP
	done()
	

func _unhelp():
	player("that’s literally the least helpful thing you could have said.")
	yield(controller, "next")
	npc("jeez, bud.")
	yield(controller, "next")
	npc("you sound a little antsy.")
	yield(controller, "next")
	npc("you totally sure you wanna do this?")
	yield(controller, "next")
	
	player_choice([
		"…okay.",
		"maybe i don’t.",
		"…"
	])
	choice = yield(controller, "next")
	
	match choice:
		0: return _findokay()
		1: return _dontbond()
		2: return _ignore()
	
	
func _ignore():
	npc("hm.")
	yield(controller, "next")
	npc("if that’s how it’s going to be, then, bye")
	yield(controller, "next")
	
	ending = endings.IGNORE
	done()



func finish():
	
	match ending:
		endings.IGNORE:
			# continue with "unable" ending
			pass
		endings.DRIFT:
			$"../../Background/DeadZone".deactivate()
			controller.get_world().quick_end_ending = "DRIFT"
			pass
		endings.GETHELP:
			$"../../Background/DeadZone".deactivate()
			$"../../Background/WarmZone".activate()
			$"../../Background/WarmerZone".activate()
			$"../../HydrogenScene".visible = true
			$"../../HydrogenScene".listening = true
	
	
	pass
	
	

func postdialogue():
	pass