extends "res://scenes/tool/dialogue/DialogueProvider.gd"

var reactivate
var choice
var share_finish = false

export (NodePath) var __diatomic
onready var diatomic = get_node(__diatomic)

func begin():
	reactivate = false
	share_finish = false

	npc("O_GREET") # oh hello!
	yield(controller, "next")
	player_choice([
		"P_GREET0", # hi there
		"P_GREET1", # hello, who are you?
		"P_IGNORE"  # ...
	])
	choice = yield(controller, "next")

	if choice == 0: # hi there
		npc("O_QBYE0") # pleased to meet you!
		yield(controller, "next")
		npc("O_QBYE1") # bye now!
		yield(controller, "next")
		reactivate = true
		return done()

	if choice == 2: return _ignore()

	npc("O_IDENT") # i am o2. who are you?
	yield(controller, "next")
	player_choice([
		"P_IDENT",   # i'm oxygen
		"P_UNSURE",  # i don't know
		"P_INQUIRE", # why are there two of you?
		"P_IGNORE"   # ...
	])
	choice = yield(controller, "next")
	match choice:
		0: return _ident()
		1: return _unsure()
		2: return _inquire()
		3: return _ignore()


func _ignore():
	npc("O_IGNORE0") # oh. hum. okay then!
	yield(controller, "next")
	npc("O_IGNORE1") # have a nice day!
	yield(controller, "next")

	done()

func _ident():
	npc("O_SAME0") # really? wow!
	yield(controller, "next")
	npc("O_SAME1") # then we're the same!
	yield(controller, "next")
	npc("O_SAME2") # why are you alone?
	yield(controller, "next")

	player_choice([
		"P_CONFUSED", # what do you mean?
		"P_NOTALONE", # i'm not alone. i have my electrons.
		"P_UNSURE",   # i don't know
		"P_IGNORE"    # ...
	])
	choice = yield(controller, "next")

	match choice:
		1: return _notalone()
		2: return _unsure()
		3: return _ignore()

	npc("O_ALONE0") # you know, you're alone!
	yield(controller, "next")
	npc("O_ALONE1") # you're oxygen, but you're not...
	yield(controller, "next")
	npc("O_ALONE2") # stable
	yield(controller, "next")

	player("P_UNSURE") # i don't know
	yield(controller, "next")

	return _unsure()

func _unsure():
	npc("O_LONELY0") # that's no good.
	yield(controller, "next")
	npc("O_LONELY1") # say, you should find something to share those electrons of yours with!
	yield(controller, "next")
	npc("O_LONELY2") # it’d be a lot less lonely.
	yield(controller, "next")

	return _to_share()

func _inquire():
	npc("O_ONE0")
	yield(controller, "next")
	npc("O_ONE1")
	yield(controller, "next")
	npc("O_ONE2")
	yield(controller, "next")
	npc("O_ONE3")
	yield(controller, "next")
	npc("O_ONE4")
	yield(controller, "next")

	player_choice([
		"P_UNSURE",   # i don't know
		"P_NOTALONE", # i'm not alone. i have my electrons.
		"P_IGNORE"
	])
	choice = yield(controller, "next")

	match choice:
		0: return _unsure()
		1: return _notalone()
		2: return _ignore()

func _notalone():
	npc("O_ELECTRON0")
	yield(controller, "next")
	npc("O_ELECTRON1")
	yield(controller, "next")
	npc("O_ELECTRON2")
	yield(controller, "next")
	npc("O_ELECTRON3")
	yield(controller, "next")
	return _to_share()

func _to_share():
	player_choice([
		"P_SHAREOK",  # sure, why not
		"P_SHAREWHY", # why would i share my electrons?
		"P_IGNORE"
	])
	choice = yield(controller, "next")

	match choice:
		0: return _share()
		2: return _ignore()

	npc("O_BURDEN0") # an electron is an awful stressful burden on you alone when it's not stable.
	yield(controller, "next")
	npc("O_BURDEN1") # you ought to ease that stress.
	yield(controller, "next")

	player("P_SHAREOK") # sure, why not
	yield(controller, "next")

	return _share()

func _share():
	npc("O_HELP0") # great!
	yield(controller, "next")
	npc("O_HELP1") # i’m going to point you in the direction of someone i know.
	yield(controller, "next")
	npc("O_HELP2") # they might be able to help with your situation.
	yield(controller, "next")

	player("P_THANK") # thank you!
	yield(controller, "next")

	npc("O_HELP3") # oh, and by the way…
	yield(controller, "next")
	npc("O_HELP4") # don’t bother talking to any nitrogen.
	yield(controller, "next")
	npc("O_HELP5") # it’s tooootally inert!
	yield(controller, "next")

	share_finish = true

	done()

#######

func finish():
	if share_finish:
		$ParticlesPath.visible = true
		$ParticlesPath.emitting = true
		controller.get_player().force_jump_to(7325, true)
		$"../../CarbonDioxideScene/DialogueProvider".is_primed = true
		$"../../Background/ColdZone".deactivate()

func postdialogue():
	if reactivate:
		controller.listening = true
		controller._player_here(controller.get_player())
	else:
		diatomic.get_node("AnimationPlayer").play("leave")
	return false