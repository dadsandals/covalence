extends TextureRect

export var max_delta = 0.0
export var active = true

onready var _world = $"/root".get_child(0)
onready var playerpf = $"../../PlayerPath/PathFollow2D"
var center
var radius

func _ready():
	radius = rect_size.x / 2
	center = rect_global_position + rect_size / 2
	
func _get_distance():
	return center.distance_to(playerpf.global_position)

func _process(delta):
	if (!active): return
	var dist = _get_distance()
	if (dist < radius):
		_world.add_speed_offset(max_delta * (1 - (dist / radius)))
		
func deactivate():
	active = false
	visible = false
	
func activate():
	active = true
	visible = true
