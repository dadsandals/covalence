hello! in this folder we (will) have some different things related to the story
and dialogue planning for covalence.

since [twine](https://twinery.org/) hides the actual files from you, we have to export the twine stories
to get them into this repository. so it may not be the latest version, but if you
want to look around you can import them into twine.