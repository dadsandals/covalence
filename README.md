# covalence
covalence is a game we're making about relationships and shitty chemistry.

- if you're looking to browse the code, you're in the right place; everything
  is in [game](https://gitlab.com/dadsandals/covalence/tree/master/game).
- if you want to look at our notes/story/plot stuff, some of that is in the
  [story folder](https://gitlab.com/dadsandals/covalence/tree/master/story)
  and other things might be [in the wiki](https://gitlab.com/dadsandals/covalence/wikis/home).
- if you want to check on our progress, we're using the
  [gitlab issue board](https://gitlab.com/dadsandals/covalence/boards) to keep
  track of tasks and [GanttProject](https://www.ganttproject.biz/) to try to
  manage our schedule, more or less - that's checked into the repository in
  [schedule.gan](https://gitlab.com/dadsandals/covalence/blob/master/schedule.gan).